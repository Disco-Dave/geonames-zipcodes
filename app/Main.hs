module Main (main) where

import qualified Geonames.ZipCodes as ZipCodes


main :: IO ()
main =
  ZipCodes.main
