module Geonames.ZipCodes (main) where

import Control.Monad (join)
import Geonames.ZipCodes.Command (Command (..))
import qualified Geonames.ZipCodes.Command as Command
import qualified Geonames.ZipCodes.Config as Config
import Geonames.ZipCodes.Database.Migrate (migrate)
import Geonames.ZipCodes.Database.Update (update)
import Geonames.ZipCodes.Http (startHttp)


main :: IO ()
main = do
  command <- Command.fromArgs

  case command of
    Migrate ->
      migrate =<< Config.fromEnvironment Config.databaseUrl
    Update ->
      update =<< Config.fromEnvironment Config.databaseUrl
    StartHttp ->
      join . Config.fromEnvironment $
        startHttp
          <$> Config.httpPort
          <*> Config.databaseUrl
