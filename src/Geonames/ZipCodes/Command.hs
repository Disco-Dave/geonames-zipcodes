module Geonames.ZipCodes.Command (
  Command (..),
  fromArgs,
) where

import Options.Applicative


data Command
  = Migrate
  | Update
  | StartHttp
  deriving (Show, Eq)


commandParser :: Parser Command
commandParser =
  subparser . mconcat $
    [ command "migrate" (info (pure Migrate) (progDesc "Migrate the postgresql database"))
    , command "update" (info (pure Update) (progDesc "Update the database with the latest data from geonames"))
    , command "start-http" (info (pure StartHttp) (progDesc "Start the http server"))
    ]


fromArgs :: IO Command
fromArgs =
  execParser $
    info
      (commandParser <**> helper)
      ( fullDesc
          <> header "geonames-zipcodes - rest api for geonames zip codes"
      )
