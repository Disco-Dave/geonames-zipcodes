{-# LANGUAGE QuasiQuotes #-}

module Geonames.ZipCodes.Database.Update (update) where

import Codec.Archive.Zip.Conduit.UnZip (ZipEntry (zipEntryName), unZipStream)
import Conduit
import Control.Monad (void)
import Data.ByteString (ByteString)
import Data.CSV.Conduit (CSV (intoCSV), CSVSettings (csvSep), defCSVSettings)
import Data.Pool (Pool)
import qualified Data.Pool as Pool
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Encoding (encodeUtf8)
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import qualified Database.PostgreSQL.Simple as Postgres
import Database.PostgreSQL.Simple.SqlQQ (sql)
import Geonames.ZipCodes.Database.Pool (withPool)
import Network.HTTP.Simple (getResponseBody, httpSource)


data Record = Record
  { zipCode :: Text
  , city :: Text
  , state :: Text
  }
  deriving (Show, Eq)


downloadFile :: ConduitT i ByteString (ResourceT IO) ()
downloadFile =
  let takeFile fileName = do
        let entryFileName ze =
              either encodeUtf8 id ze.zipEntryName

        dropWhileC $ \case
          Left ze -> entryFileName ze /= fileName
          Right _ -> True

        takeWhileC $ \case
          Left ze -> entryFileName ze == fileName
          Right _ -> True
   in httpSource "https://download.geonames.org/export/zip/US.zip" getResponseBody
        .| void unZipStream
        .| takeFile "US.txt"
        .| concatMapC (either (const Nothing) Just)


parseFile :: ConduitT ByteString Record (ResourceT IO) ()
parseFile =
  let parseRow = \case
        (_ : zipCode : city : _ : state : _)
          | not (any Text.null [zipCode, city, state]) ->
              Just Record{..}
        _ ->
          Nothing
   in intoCSV (defCSVSettings{csvSep = '\t'}) .| concatMapC parseRow


insertRecords :: Pool Postgres.Connection -> ConduitT Record Void (ResourceT IO) ()
insertRecords pool =
  let toParams r =
        ( r.zipCode
        , r.city
        , r.state
        )
      upsert v =
        let query =
              [sql|
                INSERT INTO public.zip_codes
                (
                  zip_code
                  ,city
                  ,state
                )
                VALUES
                ( 
                  ?
                  ,?
                  ,?
                )
                ON CONFLICT (zip_code) DO NOTHING;
              |]
            params = Vector.toList v
         in liftIO . void . Pool.withResource pool $ \connection ->
              Postgres.executeMany connection query params
   in mapC toParams
        .| conduitVector @Vector 500
        .| iterMC upsert
        .| sinkNull


update :: ByteString -> IO ()
update databaseUrl = withPool databaseUrl $ \pool ->
  runConduitRes $
    downloadFile
      .| parseFile
      .| insertRecords pool
