CREATE TABLE public.zip_codes (
  zip_code text not null primary key
  ,city text not null
  ,state text not null
);
