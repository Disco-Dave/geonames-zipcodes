module Geonames.ZipCodes.Database.AnyField (
  AnyField (..),
) where

import Database.PostgreSQL.Simple.ToField (ToField (..))


data AnyField where
  AnyField :: ToField a => a -> AnyField


instance ToField AnyField where
  toField (AnyField a) =
    toField a
