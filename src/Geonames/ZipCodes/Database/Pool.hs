module Geonames.ZipCodes.Database.Pool (withPool) where

import Data.ByteString (ByteString)
import Data.Pool (Pool)
import qualified Data.Pool as Pool
import qualified Database.PostgreSQL.Simple as Postgres
import UnliftIO.Exception (bracket)


withPool :: ByteString -> (Pool Postgres.Connection -> IO a) -> IO a
withPool databaseUrl =
  let createConnection =
        Postgres.connectPostgreSQL databaseUrl
      freeConnection =
        Postgres.close

      createPool =
        Pool.newPool
          Pool.PoolConfig
            { createResource = createConnection
            , freeResource = freeConnection
            , poolCacheTTL = 60
            , poolMaxResources = 3
            }
      freePool =
        Pool.destroyAllResources
   in bracket createPool freePool
