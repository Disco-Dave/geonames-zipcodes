module Geonames.ZipCodes.Database.Migrate (migrate) where

import Data.ByteString (ByteString)
import qualified Database.PostgreSQL.Simple as Postgres
import Database.PostgreSQL.Simple.Migration (MigrationCommand (..), MigrationResult (..), defaultOptions, runMigration)
import Paths_geonames_zipcodes (getDataFileName)
import UnliftIO.Exception (bracket, throwString)


migrate :: ByteString -> IO ()
migrate databaseUrl = do
  migrationsDir <- getDataFileName "src/Geonames/ZipCodes/Database/Migrations"

  result <-
    let command =
          mconcat
            [ MigrationInitialization
            , MigrationDirectory migrationsDir
            ]
     in bracket (Postgres.connectPostgreSQL databaseUrl) Postgres.close $ \connection ->
          runMigration connection defaultOptions command

  case result of
    MigrationError err -> throwString err
    MigrationSuccess -> pure ()
