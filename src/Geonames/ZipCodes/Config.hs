module Geonames.ZipCodes.Config (
  databaseUrl,
  httpPort,
  fromEnvironment,
) where

import Data.ByteString (ByteString)
import qualified Env
import qualified Network.Wai.Handler.Warp as Warp


databaseUrl :: Env.Parser Env.Error ByteString
databaseUrl =
  Env.var Env.str "DATABASE_URL" (Env.help "URL used to connect to postgresql database.")


httpPort :: Env.Parser Env.Error Warp.Port
httpPort =
  Env.var Env.auto "HTTP_PORT" (Env.help "Port to start the http sever on.")


fromEnvironment :: Env.Parser Env.Error config -> IO config
fromEnvironment =
  Env.parse (Env.header "genonames-zipcodes")
