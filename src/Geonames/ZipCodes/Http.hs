{-# LANGUAGE QuasiQuotes #-}

module Geonames.ZipCodes.Http (startHttp) where

import Conduit (liftIO)
import Control.Lens ((.~), (?~))
import qualified Data.Aeson as Aeson
import Data.ByteString (ByteString)
import Data.Function ((&))
import Data.Maybe (catMaybes)
import Data.OpenApi (OpenApi)
import qualified Data.OpenApi as OpenApi
import Data.Pool (Pool)
import qualified Data.Pool as Pool
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Version (showVersion)
import qualified Database.PostgreSQL.Simple as Postgres
import Database.PostgreSQL.Simple.FromRow (field)
import Database.PostgreSQL.Simple.SqlQQ (sql)
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import Geonames.ZipCodes.Database.AnyField (AnyField (..))
import Geonames.ZipCodes.Database.Pool (withPool)
import qualified Network.Wai.Handler.Warp as Warp
import Paths_geonames_zipcodes (version)
import Servant
import Servant.OpenApi
import Servant.Swagger.UI


newtype ZipCode = ZipCode
  { toText :: Text
  }
  deriving
    ( Show
    , Eq
    , Ord
    , FromHttpApiData
    , ToHttpApiData
    , Aeson.ToJSON
    , Aeson.FromJSON
    )


instance OpenApi.ToParamSchema ZipCode where
  toParamSchema _ =
    OpenApi.toParamSchema @Text Proxy
      & OpenApi.title ?~ "ZipCode"
      & OpenApi.description ?~ "Five digit US zip code."
      & OpenApi.example ?~ Aeson.toJSON (ZipCode "17030")


instance OpenApi.ToSchema ZipCode where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "ZipCode") $
      OpenApi.toParamSchema @ZipCode Proxy


newtype City = City
  { toText :: Text
  }
  deriving
    ( Show
    , Eq
    , FromHttpApiData
    , ToHttpApiData
    , Aeson.ToJSON
    , Aeson.FromJSON
    )


instance OpenApi.ToParamSchema City where
  toParamSchema _ =
    OpenApi.toParamSchema @Text Proxy
      & OpenApi.title ?~ "City"
      & OpenApi.description ?~ "City the zip code is associated with."
      & OpenApi.example ?~ Aeson.toJSON (City "Gratz")


instance OpenApi.ToSchema City where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "City") $
      OpenApi.toParamSchema @City Proxy


newtype State = State
  { toText :: Text
  }
  deriving
    ( Show
    , Eq
    , FromHttpApiData
    , ToHttpApiData
    , Aeson.ToJSON
    , Aeson.FromJSON
    )


instance OpenApi.ToParamSchema State where
  toParamSchema _ =
    OpenApi.toParamSchema @Text Proxy
      & OpenApi.title ?~ "State"
      & OpenApi.description ?~ "Two character state abbreviation."
      & OpenApi.example ?~ Aeson.toJSON (City "PA")


instance OpenApi.ToSchema State where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "State") $
      OpenApi.toParamSchema @State Proxy


newtype Limit = Limit
  { toNatural :: Natural
  }
  deriving
    ( Show
    , Eq
    , Ord
    , FromHttpApiData
    , ToHttpApiData
    , Aeson.ToJSON
    , Aeson.FromJSON
    )


getLimit :: Maybe Limit -> Integer
getLimit = \case
  Nothing -> defaultLimit
  Just limit
    | limit.toNatural > maxLimit -> fromIntegral maxLimit
    | limit.toNatural < minLimit -> fromIntegral minLimit
    | otherwise -> fromIntegral $ limit.toNatural
 where
  defaultLimit = 100
  minLimit = 10
  maxLimit = 300


instance OpenApi.ToParamSchema Limit where
  toParamSchema _ =
    OpenApi.toParamSchema @Natural Proxy
      & OpenApi.title ?~ "Limit"
      & OpenApi.description ?~ "Upper limit of rows that can be returned at once. Max is 300, min is 10, and default is 100."
      & OpenApi.example ?~ Aeson.toJSON (Limit 100)


instance OpenApi.ToSchema Limit where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "Limit") $
      OpenApi.toParamSchema @State Proxy


newtype LastSeenZipCode = LastSeenZipCode
  { toZipCode :: ZipCode
  }
  deriving
    ( Show
    , Eq
    , Ord
    , FromHttpApiData
    , ToHttpApiData
    , Aeson.ToJSON
    , Aeson.FromJSON
    )


instance OpenApi.ToParamSchema LastSeenZipCode where
  toParamSchema _ =
    OpenApi.toParamSchema @ZipCode Proxy
      & OpenApi.title ?~ "LastSeenZipCode"
      & OpenApi.description ?~ "Last zip code from a previous call. Combine this with the limit parameter for paging."
      & OpenApi.example ?~ Aeson.toJSON (ZipCode "20001")


instance OpenApi.ToSchema LastSeenZipCode where
  declareNamedSchema _ =
    pure . OpenApi.NamedSchema (Just "LastSeenZipCode") $
      OpenApi.toParamSchema @LastSeenZipCode Proxy


data ZipCodeInfo = ZipCodeInfo
  { zipCode :: ZipCode
  , city :: City
  , state :: State
  }
  deriving (Show, Eq, Generic)
instance Aeson.ToJSON ZipCodeInfo
instance Aeson.FromJSON ZipCodeInfo
instance OpenApi.ToSchema ZipCodeInfo


type GetIndex =
  Summary "Query for zip code, city, and state."
    :> QueryParam "zipCode" ZipCode
    :> QueryParam "city" City
    :> QueryParam "state" State
    :> QueryParam "limit" Limit
    :> QueryParam "lastSeenZipCode" LastSeenZipCode
    :> Get '[JSON] [ZipCodeInfo]


getIndex ::
  Pool Postgres.Connection ->
  Maybe ZipCode ->
  Maybe City ->
  Maybe State ->
  Maybe Limit ->
  Maybe LastSeenZipCode ->
  Handler [ZipCodeInfo]
getIndex pool zipCodeQueryParam cityQueryParam stateQueryParam limitQueryParam lastSeenQueryParam = do
  let (zipCodeQuery, zipCodeParam) =
        case (zipCodeQueryParam, lastSeenQueryParam) of
          (Just zipCode, _) -> (" AND zip_code = ? ", Just $ AnyField zipCode.toText)
          (_, Just lastSeen) -> (" AND zip_code > ? ", Just $ AnyField lastSeen.toZipCode.toText)
          _ -> ("", Nothing)

      (cityQuery, cityParam) =
        case cityQueryParam of
          Just city -> (" AND city = ? ", Just $ AnyField city.toText)
          Nothing -> ("", Nothing)

      (stateQuery, stateParam) =
        case stateQueryParam of
          Just state -> (" AND state = ? ", Just $ AnyField state.toText)
          Nothing -> ("", Nothing)

      (limitQuery, limitParam) =
        (" LIMIT ? ", AnyField $ getLimit limitQueryParam)

      rowParser = do
        zipCode <- fmap ZipCode field
        city <- fmap City field
        state <- fmap State field
        pure ZipCodeInfo{..}

      baseQuery =
        [sql|
          SELECT
            zip_code
            ,city
            ,state
          FROM public.zip_codes
          WHERE 1 = 1
        |]

      query =
        mconcat
          [ baseQuery
          , zipCodeQuery
          , cityQuery
          , stateQuery
          , " ORDER BY zip_code "
          , limitQuery
          , ";"
          ]

      params =
        catMaybes
          [ zipCodeParam
          , cityParam
          , stateParam
          , Just limitParam
          ]

  liftIO . Pool.withResource pool $ \connection ->
    Postgres.queryWith rowParser connection query params


type Api =
  GetIndex


server :: Pool Postgres.Connection -> Server Api
server =
  getIndex


type ApiWithSwagger =
  SwaggerSchemaUI "docs" "openapi.json" :<|> Api


openApi :: OpenApi
openApi =
  toOpenApi @Api Proxy
    & OpenApi.info . OpenApi.title .~ "Zip Codes from Geonames"
    & OpenApi.info . OpenApi.description ?~ "REST API for querying zip codes from Geonames."
    & OpenApi.info . OpenApi.version .~ Text.pack (showVersion version)


serverWithSwagger :: Pool Postgres.Connection -> Server ApiWithSwagger
serverWithSwagger pool =
  swaggerSchemaUIServer openApi :<|> server pool


startHttp :: Warp.Port -> ByteString -> IO ()
startHttp port databaseUrl =
  withPool databaseUrl $ \pool ->
    let application = serve @ApiWithSwagger Proxy (serverWithSwagger pool)
        settings =
          Warp.defaultSettings
            & Warp.setPort port
            & Warp.setBeforeMainLoop (putStrLn $ "Vist docs at http://localhost:" <> show port <> "/docs")
     in Warp.runSettings settings application
